import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


class cars_generating():
    def __init__(self, max_features, n_points):
        self.max_features = max_features
        self.n_points = n_points
        self.nu = np.linspace(0, 1, self.n_points)

    ##################################################################
    def random_chi3(self):
        """
        generates a random spectrum, without NRB.
        output:
            params =  matrix of parameters. each row corresponds to the [amplitude, resonance, linewidth] of each generated feature (n_lor,3)
        """
        n_lor = self.max_features
        a = np.random.uniform(0, 1, n_lor)  # amplitude
        w = np.random.uniform(0, 1, n_lor)  # resonance frequency
        g = np.random.uniform(0.001, 0.008, n_lor)  # line width
        # sorting the matrix by w
        params = np.c_[a, w, g]
        sorted_params = params[np.argsort(params[:, 1])]
        return sorted_params

    ##############################################################
    def build_chi3(self, params):
        """
        buiilds the normalized chi3 complex vector
        inputs:
            params: (n_lor, 3)
        outputs
            chi3: complex, (n_points, )
        """

        chi3 = np.sum(params[:, 0] / (-self.nu[:, np.newaxis] + params[:, 1] - 1j * params[:, 2]), axis=1)

        return chi3 / np.max(np.abs(chi3))
        ###############################################################

    def sigmoid(self, x, c, b):
        return 1 / (1 + np.exp(-(x - c) * b))

    ###############################################################
    def generate_nrb(self):
        """
        Produces a normalized shape for the NRB
        outputs
            NRB: (n_points,)
        """
        bs = np.random.normal(10, 5, 2)
        c1 = np.random.normal(0.2, 0.3)
        c2 = np.random.normal(0.7, .3)
        cs = np.r_[c1, c2]
        sig1 = self.sigmoid(self.nu, cs[0], bs[0])
        sig2 = self.sigmoid(self.nu, cs[1], -bs[1])
        nrb = sig1 * sig2
        return nrb

    ################################################################
    def get_spectrum(self):
        """
        Produces a cars spectrum.
        It outputs the normalized cars and the corresponding imaginary part.
        Outputs
            cars: (n_points,)
            chi3.imag: (n_points,)
        """
        rdchi_3 = self.random_chi3()
        chi3 = self.build_chi3(rdchi_3) * np.random.uniform(0.3, 1)
        nrb = self.generate_nrb()
        noise = np.random.randn(self.n_points) * np.random.uniform(0.0005, 0.003)
        cars = ((np.abs(chi3 + nrb) ** 2) / 2 + noise)
        return cars, chi3.imag, rdchi_3

    ################################################################
    def generate_batch(self, size=4000):
        X = np.empty((size, self.n_points))
        y = np.empty((size, self.n_points))
        # z=np.empty((size, n_points,1))
        # X, y = get_spectrum()
        for i in range(size):
            X[i, :], y[i, :], z = self.get_spectrum()
        return X, y